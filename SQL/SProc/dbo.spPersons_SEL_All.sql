-- Check if stored procedure exists, and delete it if it does.
IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = OBJECT_ID('dbo.spPersons_SEL_ALL')
              AND OBJECTPROPERTY(id, 'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE dbo.spPersons_SEL_ALL
  END
GO
-- Set quoted identifiers on, in case any column is a reserved word.
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spPersons_SEL_ALL
AS
SET NOCOUNT ON 
  SELECT
   code
  ,name
  ,surname
  ,id_number  
FROM dbo.Persons WITH(NOLOCK)
