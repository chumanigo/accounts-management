-- Check if stored procedure exists, and delete it if it does.
IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = OBJECT_ID('dbo.spTransactions_SEL_ALL')
              AND OBJECTPROPERTY(id, 'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE dbo.spTransactions_SEL_ALL
  END
GO
-- Set quoted identifiers on, in case any column is a reserved word.
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spTransactions_SEL_ALL
AS
SET NOCOUNT ON 
  SELECT
   code
  ,account_code
  ,transaction_date
  ,capture_date
  ,amount
  ,description  
FROM dbo.Transactions WITH(NOLOCK)
