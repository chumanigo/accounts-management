-- Check if stored procedure exists, and delete it if it does.
IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = OBJECT_ID('dbo.spPersons_SEL_IDNumber_Surname_AccountNumber')
              AND OBJECTPROPERTY(id, 'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE dbo.spPersons_SEL_IDNumber_Surname_AccountNumber
  END
GO
-- Set quoted identifiers on, in case any column is a reserved word.
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spPersons_SEL_IDNumber_Surname_AccountNumber
(
  @id_number VARCHAR(50)
 ,@surname VARCHAR(50)
 ,@account_number VARCHAR(50)
)
AS
SET NOCOUNT ON 
  SELECT DISTINCT
   p.code 
  ,p.name
  ,p.surname 
  ,p.id_number
FROM   dbo.Persons p WITH(NOLOCK)
LEFT JOIN dbo.Accounts ac WITH(NOLOCK)
        ON ac.person_code = p.code
WHERE p.id_number = @id_number
   OR p.surname = @surname
   OR ac.account_number = @account_number
GO 