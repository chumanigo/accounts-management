﻿
toastr.options.timeOut = 4000;
toastr.options.extendedTimeOut = 4000;
toastr.options.positionClass = 'toast-top-center'  

function ValidatePerson() {
    var isAllValid = true;
    if (!($('#idnumber').val().trim() != '') || 0) {
        isAllValid = false;
        $('#idnumber').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#idnumber').siblings('span.error').css('visibility', 'hidden');
    }
    return isAllValid;
}

function isValidDate(dateString) {

    var currentdate = new Date();    

    // mm-dd-yyyy hh:mm:ss     
    var regex = /(\d{1,2})[-\/](\d{1,2})[-\/](\d{4})\s*(\d{0,2}):?(\d{0,2}):?(\d{0,2})/;
    parts = regex.exec(dateString);

    if (parts) {        
        var date = new Date((+parts[3]), (+parts[1]) - 1, (+parts[2]), (+parts[4]), (+parts[5]), (+parts[6]));
        if ((date.getDate() == parts[2]) && (date.getMonth() == parts[1] - 1) && (date.getFullYear() == parts[3])) {            

            // reject date greater than today
            if (date.getDay() > currentdate.getDay()) {
                return false;
            }
            return date;
        }
    }

    return false;

}

function ValidateTransaction()
{
    var isAllValid = true;
    if (!($('#description').val().trim() != '') || 0) {
        isAllValid = false;
        $('#description').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#description').siblings('span.error').css('visibility', 'hidden');
    }

    if (!($('#transactiondate').val() != '') || 0) {
        isAllValid = false;
        $('#transactiondate').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#transactiondate').siblings('span.error').css('visibility', 'hidden');
    }

    if (!isValidDate($('#transactiondate').val())) {

        //toastr.info($('#transactiondate').val(), " Date selected");
    }

    if (!($('#amount').val().trim() != '' && !isNaN($('#amount').val().trim())) || $('#amount').val().trim() == '0' ) {
        isAllValid = false;
        $('#amount').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#amount').siblings('span.error').css('visibility', 'hidden');
    }

    return isAllValid;
}

function CreatePerson() {

    if (!ValidatePerson())
        return;

    var personData =
    {
        name: $('#name').val().trim(),
        surname: $('#surname').val().trim(),
        id_number: $('#idnumber').val().trim()
    }

    $.ajax({
        type: "POST",
        url:'/Persons/CreatePerson', //'@Url.Action("CreatePerson", "Persons")',
        data: personData,
        success: function (result) {
            if (result.Success == true) {
                $('.createPersonModal').modal('hide');
                toastr.success(result.Message);
                setInterval('location.reload()', 3000);
            }
            else {
                toastr.error(result.Message);
            }

        },
        error: function (xhr, error) {
            toastr.error("An error occured while trying to create a person. Status: " + xhr.status + ", Error: " + xhr.statusText);
        }
    });
}

function UpdatePerson(personCode, idNumber, name, surname) {
    var newName = $('#name').val();
    var newSurname = $('#surname').val();    
    
    var personData =
    {
        code: personCode,
        name: newName,
        surname: newSurname,
        id_number: idNumber
    }

    if (idNumber=='')
        return;

    $.ajax({
        type: "POST",
        url: '/Persons/UpdatePerson', //'@Url.Action("UpdatePerson", "Persons")',
        data: personData,
        success: function (result) {
            if (result.Success == true) {                
                toastr.success(result.Message);                
            }
            else {
                toastr.error(result.Message);
            }

        },
        error: function (xhr, error) {
            toastr.error("An error occured while trying to update a person. Status: " + xhr.status + ", Error: " + xhr.statusText);
        }
    });
}

function DeletePerson(personCode) {
    $.ajax({
        url: '/Persons/DeletePerson',//@Url.Action("DeletePerson", "Persons")',
        cache: false,
        type: 'post',
        datatype: "json",
        data: {
            code: personCode
        },
        success: function (result) {
            $('.modal').modal('hide');
            if (result.Success == true) {
                toastr.success(result.Message);
                setInterval('location.reload()', 3000);
            }
            else {
                toastr.error(result.Message);
            }            
        },
        error: function (xhr, error) {
            toastr.error("An error occured while trying to delete the selected person. Status: " + xhr.status + ", Error: " + xhr.statusText);
        }
    });
}

function ValidateAccount() {
    var isAllValid = true;
    if (!($('#accountnumber').val().trim() != '' && (parseInt($('#accountnumber').val()) || 0))) {
        isAllValid = false;
        $('#accountnumber').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#accountnumber').siblings('span.error').css('visibility', 'hidden');
    }

    if (!($('#outstandingbalance').val().trim() != '' && (parseInt($('#outstandingbalance').val()) || 0))) {
        isAllValid = false;
        $('#outstandingbalance').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#outstandingbalance').siblings('span.error').css('visibility', 'hidden');
    }
    return isAllValid;
}

function UpdateAccount(accountCode) {    
    
    if (!($('#accountnumber').val().trim() != '' && (parseInt($('#accountnumber').val()) || 0))) {        
         $('#accountnumber').siblings('span.error').css('visibility', 'visible');
        return;
    }
    else {
        $('#accountnumber').siblings('span.error').css('visibility', 'hidden');
    }

    var accountData =
    {
        code: accountCode,
        account_number: $('#accountnumber').val().trim(),
        outstanding_balance: $('#outstandingbalance').val()
    }

    $.ajax({
        type: "POST",
        url: '/Accounts/UpdateAccount',//'@Url.Action("UpdateAccount", "Accounts")',
        data: accountData,
        success: function (result) {
            if (result.Success == true) {
                //$('.createAccountModal').modal('hide');
                toastr.success(result.Message);
                //setInterval('location.reload()', 3000);
            }
            else {
                toastr.error(result.Message);
            }

        },
        error: function (xhr, error) {
            toastr.error("An error occured while trying to update account. Status: " + xhr.status + ", Error: " + xhr.statusText);
        }
    });
}

function CreateAccount(personCode) {
    //var personCode = '@Model.Person.code';

    if (!ValidateAccount())
        return;

    var accountData =
    {
        person_code: personCode,
        acount_active : true,
        account_number: $('#accountnumber').val().trim(),
        outstanding_balance: $('#outstandingbalance').val()
    }

    $.ajax({
        type: "POST",
        url: '/Accounts/CreateAccount',//'@Url.Action("CreateAccount", "Accounts")',
        data: accountData,
        success: function (result) {
            if (result.Success == true) {
                $('.createAccountModal').modal('hide');
                toastr.success(result.Message);
                setInterval('location.reload()', 3000);
            }
            else {
                toastr.error(result.Message);
            }

        },
        error: function (xhr, error) {
            toastr.error("An error occured while trying to create account. Status: " + xhr.status + ", Error: " + xhr.statusText);
        }
    });
}

function CloseAccount(accountCode) {
    var accountData =
    {
        code: accountCode
    }

    $.ajax({
        type: "POST",
        url: '/Accounts/CloseAccount',
        data: accountData,
        success: function (result) {
            if (result.Success == true) {
                toastr.success(result.Message);
            }
            else {
                toastr.error(result.Message);
            }
        },
        error: function (xhr, error) {
            toastr.error("An error occured while trying to close account. Status: " + xhr.status + ", Error: " + xhr.statusText);
        }
    });
}

function UpdateTransaction(code, transactionDate) {

    var isAllValid = true;
    if (!($('#description').val().trim() != '') || 0) {
        isAllValid = false;
        $('#description').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#description').siblings('span.error').css('visibility', 'hidden');
    }

    if (!($('#transactiondate').val() != '') || 0) {
        isAllValid = false;
        $('#transactiondate').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#transactiondate').siblings('span.error').css('visibility', 'hidden');
    }

    if (!isAllValid)
        return;

    var transactionData =
    {
        code: code,
        transaction_date: transactionDate,
        description: $('#description').val()
    }

    $.ajax({
        type: "POST",
        url: '/Transactions/UpdateTransaction', //'@Url.Action("UpdateTransaction", "Transactions")',
        data: transactionData,
        success: function (result) {
            if (result.Success == true) {
                toastr.success(result.Message);
                setInterval('location.reload()', 3000);
            }
            else {
                toastr.error(result.Message);
            }

        },
        error: function (xhr, error) {
            toastr.error("An error occured while trying to update account. Status: " + xhr.status + ", Error: " + xhr.statusText);
        }
    });
}