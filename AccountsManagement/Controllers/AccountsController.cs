﻿using AccountsManagement.DAL.Interface;
using AccountsManagement.Models.ViewModel;
using PagedList;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCAssessment.Controllers
{
    public class AccountsController : Controller
    {
        #region Constructor
        private IDataAccess _dataAccess { get; set; }

        public AccountsController(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CreateAccount(AccountsViewModel account)
        {
            if (ModelState.IsValid)
            {                
                Tuple<bool, string> results = _dataAccess.SaveAccount(account);
                return Json(new { Success = results.Item1, Message = results.Item2 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid Account object." }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult CloseAccount(AccountsViewModel account)
        {
            if (ModelState.IsValid)
            {                
                Tuple<bool, string> results = _dataAccess.CloseAccount(account.code);
                return Json(new { Success = results.Item1, Message = results.Item2 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid Account object." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ReOpenAccount(AccountsViewModel account)
        {
            if (ModelState.IsValid)
            {
                Tuple<bool, string> results = _dataAccess.ReOpenAccount(account.code);
                return Json(new { Success = results.Item1, Message = results.Item2 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid Account object." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddAccount(PersonAccountsViewModel account)
        {
            if (account.Account != null)
            {
                int itemNo = account.Accounts.Count;
                Tuple<bool, string> results = _dataAccess.SaveAccount(account.Account);
                ViewBag.Message = results.Item2;
            }

            return PartialView("Edit","Persons");
        }

        [HttpPost]
        public ActionResult Edit(AccountsViewModel model, string action, int code, int? pageNumber)
        {            
            if(action.ToLower() == "edit")
            {         
                AccountsViewModel account = _dataAccess.GetAllAccounts().Where(x => x.code == code).FirstOrDefault();

                AccountTransactionsViewModel viewModel = new AccountTransactionsViewModel
                {
                    Account = account,
                    Transaction = new TransactionsViewModel { account_code = account.code, transaction_date = DateTime.Now},
                    Transactions = _dataAccess.GetTransactionsByAccountCode(code).ToList()
                };

                return View("Edit", viewModel);        

            }

            var data = _dataAccess.GetAllAccounts().ToPagedList(pageNumber ?? 1, 10);
            return View("Index", data);
        }

        [HttpPost]
        public JsonResult UpdateAccount(AccountsViewModel account)
        {
            if (ModelState.IsValid)
            {
                Tuple<bool, string> results = _dataAccess.UpdateAccount(account);
                return Json(new { Success = results.Item1, Message = results.Item2 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid Person object." }, JsonRequestBehavior.AllowGet);
        }
    }
}