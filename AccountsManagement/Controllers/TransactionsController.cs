﻿using AccountsManagement.DAL.Interface;
using AccountsManagement.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCAssessment.Controllers
{
    public class TransactionsController : Controller
    {
        #region Constructor
        private IDataAccess _dataAccess { get; set; }

        public TransactionsController(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CreateTransaction(TransactionsViewModel transaction)
        {
            if (ModelState.IsValid)
            {                
                Tuple<bool, string> results = _dataAccess.SaveTransaction(transaction);
                return Json(new { Success = results.Item1, Message = results.Item2 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid Transaction object." }, JsonRequestBehavior.AllowGet);
        }
   
        public JsonResult UpdateTransaction(TransactionsViewModel transaction)
        {
            if (ModelState.IsValid)
            {
                Tuple<bool, string> results = _dataAccess.UpdateTransaction(transaction);
                return Json(new { Success = results.Item1, Message = results.Item2 }, JsonRequestBehavior.AllowGet);
            } 

            return Json(new { Success = false, Message = "Invalid Transaction object." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Edit(int code)
        {
             TransactionsViewModel transaction = _dataAccess.GetAllTransactions().Where(x => x.code == code).FirstOrDefault();
             return View(transaction);
        }
    }
}