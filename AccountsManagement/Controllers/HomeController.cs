﻿using AccountsManagement.DAL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccountsManagement.Controllers
{
    public class HomeController : Controller
    {

        #region Constructor
        private IDataAccess _dataAccess { get; set; }

        public HomeController(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        } 
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Application description page.";

            return View();
        }

        public ActionResult Persons()
        {
            ViewBag.Message = "Persons";

            var data = _dataAccess.GetAllPersons();
            return View(data);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact page.";

            return View();
        }
    }
}