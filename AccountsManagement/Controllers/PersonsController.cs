﻿using AccountsManagement.DAL.Interface;
using AccountsManagement.Models.ViewModel;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccountsManagement.Controllers
{
    public class PersonsController : Controller
    {
        #region Constructor
        private IDataAccess _dataAccess { get; set; }

        public PersonsController(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        #endregion

        // GET: Persons
        public ActionResult Index(string option, string search, int? pageNumber)
        {
            if (!string.IsNullOrEmpty(option) &&
               !string.IsNullOrEmpty(search))
            {
                string idNo = "", surname = "", accountNo = "";
                switch (option)
                {
                    case "Surname":
                        surname = search;
                        break;
                    case "AccountNo":
                        accountNo = search;
                        break;
                    case "IDNumber":
                        idNo = search;
                        break;
                    default:
                        break;
                }
                var data = _dataAccess.SearchPerson(idNo, surname, accountNo).ToPagedList(pageNumber ?? 1, 10);
                return View(data);
            }
            else
            {

                var data = _dataAccess.GetAllPersons().ToPagedList(pageNumber ?? 1, 10);

                return View(data);
            }
        }

        [HttpPost]
        public JsonResult UpdatePerson(PersonViewModel person)
        {
            if (ModelState.IsValid)
            {
                Tuple<bool, string> results = _dataAccess.UpdatePerson(person);
                return Json(new { Success = results.Item1, Message = results.Item2 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid Person object." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreatePerson(PersonViewModel person)
        {
            if (ModelState.IsValid)
            {
                Tuple<bool, string> results = _dataAccess.SavePerson(person);
                return Json(new { Success = results.Item1, Message = results.Item2 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid Person object." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Edit(PersonViewModel model, string action, int code, int? pageNumber)
        {
            switch (action.ToLower())
            {
                case "delete":
                    var status = _dataAccess.DeletePerson(code);
                    break;
                case "edit":
                    PersonViewModel person = _dataAccess.GetAllPersons().Where(x => x.code == code).FirstOrDefault();
                    PersonAccountsViewModel viewModel = new PersonAccountsViewModel
                    {
                        Person = person,
                        Account = new AccountsViewModel { person_code = person.code },
                        Accounts = _dataAccess.GetAccountsByPersonCode(code).ToList()
                    };
                    return View("Edit", viewModel);
                default:
                    break;
            }

            var data = _dataAccess.GetAllPersons().ToPagedList(pageNumber ?? 1, 10);
            return View("Index", data);
        }

        [HttpPost]
        public JsonResult DeletePerson(PersonViewModel person)
        {
            if(person != null &&
               person.code > 0)
            {
                var results = _dataAccess.DeletePerson(person.code);
                return Json(new { Success = results.Item1, Message = results.Item2 }, JsonRequestBehavior.AllowGet);
            }
                        
            return Json(new { Success = false, Message= "Invalid Person object." }, JsonRequestBehavior.AllowGet);
        }

    }
}