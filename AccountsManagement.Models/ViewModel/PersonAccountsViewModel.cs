﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountsManagement.Models.ViewModel
{
    public class PersonAccountsViewModel
    {
        public PersonViewModel Person { get; set; }

        public AccountsViewModel Account { get; set; }

        public List<AccountsViewModel> Accounts { get; set; }
    }
}
