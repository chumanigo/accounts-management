﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AccountsManagement.Models.ViewModel
{
    public class TransactionsViewModel
    {
        [Key]
        [Display(Name = "Transaction number")]
        public int code { get; set; }

        [Required]
        [Display(Name = "Account Code")]
        public int account_code { get; set; }

        [Required]
        [Display(Name = "Transaction Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime transaction_date { get; set; }

        [Required]
        [ReadOnly(true)]
        [Display(Name = "Capture Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime capture_date
        { get { return DateTime.Now; }
          set {value = DateTime.Now; }
        }
        [Required]
        [Display(Name = "Transaction Amount")]
        //[DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = true)]
        public decimal amount { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string description { get; set; }

        [Display(Name = "Transaction Type")]
        public string transaction_type { get; set; }

        //public IEnumerable<SelectListItem> Accounts { get; set; }
    }
}
