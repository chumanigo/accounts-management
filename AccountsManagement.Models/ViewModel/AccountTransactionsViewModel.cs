﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountsManagement.Models.ViewModel
{
    public class AccountTransactionsViewModel
    {
        public AccountsViewModel Account { get; set; }

        public TransactionsViewModel Transaction { get; set; }

        public List<TransactionsViewModel> Transactions { get; set; }
    }
}
