﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountsManagement.Models.ViewModel
{
    public class AccountsViewModel
    {
        public int code { get; set; }        
        public int person_code { get; set; }
        [Display(Name = "Account Number")]
        public string account_number { get; set; }
        [Display(Name = "Outstanding Balance")]
        //[DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = true)]
        public decimal outstanding_balance { get; set; }
        [Display(Name = "Account Active")]
        public bool account_active { get; set; }
    }
}
