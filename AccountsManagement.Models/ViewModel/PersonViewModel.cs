﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountsManagement.Models.ViewModel
{
    public partial class PersonViewModel
    {
        public int code { get; set; }
        [Display(Name = "Name")]
        public string name { get; set; }
        [Display(Name = "Surname")]
        public string surname { get; set; }
        [Required]
        [Display(Name = "ID Number")]
        public string id_number { get; set; }
    }
}
