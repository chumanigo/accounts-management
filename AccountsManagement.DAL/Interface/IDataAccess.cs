﻿using AccountsManagement.Models.ViewModel;
using System;
using System.Collections.Generic;

namespace AccountsManagement.DAL.Interface
{
    public interface IDataAccess
    {
        #region Person

        IEnumerable<PersonViewModel> GetAllPersons();
        Tuple<bool, string> DeletePerson(int personCode);
        IEnumerable<PersonViewModel> SearchPerson(string idNo, string surname, string accountNo);
        Tuple<bool, string> SavePerson(PersonViewModel person);
        Tuple<bool, string> UpdatePerson(PersonViewModel person);

        #endregion

        #region Account

        IEnumerable<AccountsViewModel> GetAllAccounts();
        IEnumerable<AccountsViewModel> GetAccountsByPersonCode(int personCode);
        Tuple<bool, string> SaveAccount(AccountsViewModel account);
        Tuple<bool, string> UpdateAccount(AccountsViewModel account);
        Tuple<bool, string> CloseAccount(int accountCode);
        Tuple<bool, string> ReOpenAccount(int accountCode);
        bool DeleteAccount(int accountCode);

        #endregion

        #region Transaction

        IEnumerable<TransactionsViewModel> GetAllTransactions();
        IEnumerable<TransactionsViewModel> GetTransactionsByAccountCode(int acountCode);
        Tuple<bool, string> SaveTransaction(TransactionsViewModel transaction);
        Tuple<bool, string> UpdateTransaction(TransactionsViewModel transaction); 
        bool DeleteTransaction(int transactionCode);

        #endregion
    }
}
