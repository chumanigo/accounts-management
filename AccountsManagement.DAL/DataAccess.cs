﻿using AccountsManagement.DAL.Interface;
using AccountsManagement.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;


namespace AccountsManagement.DAL
{
    public class DataAccess : IDataAccess
    {
        #region Properties
        private AccountsManagementEntities _accountsManagementEntities { get; set; }
        #endregion

        #region Constructor

        public DataAccess()
        {
            _accountsManagementEntities = new AccountsManagementEntities();
        }
        
        #endregion

        #region Persons

        public IEnumerable<PersonViewModel> GetAllPersons()
        {
            var data = _accountsManagementEntities.Database.SqlQuery<PersonViewModel>("dbo.spPersons_SEL_ALL").ToList<PersonViewModel>();            
            return data;
        }

        public Tuple<bool, string> DeletePerson(int personCode)
        {
            Tuple<bool, string> results;            
            try
            {
                //-- check if person has no accounts or all accounts are closed may be deleted.
                var accountsList = _accountsManagementEntities.Accounts.Where(x => x.person_code == personCode && x.account_active).ToList();
                if(accountsList.Count > 0)
                {
                    results = new Tuple<bool, string>(false, "Cannot delete person with active account(s)."); 
                    return results;
                }

                Person personObj = new Person { code = personCode };
                _accountsManagementEntities.Entry(personObj).State = EntityState.Deleted;
                _accountsManagementEntities.SaveChanges();
                results = new Tuple<bool, string>(true, "Succesfully deleted the person.");
            }
            catch (Exception)
            {
                results = new Tuple<bool, string>(false, "Failed deleting person.");                
            }

            return results;
        }
        
        public Tuple<bool, string> SavePerson(PersonViewModel person)
        {
            Tuple<bool, string> results;
            try
            {                
                //validate duplicate id number
                var personObj = _accountsManagementEntities
                    .Persons.Where(x => x.id_number == person.id_number).FirstOrDefault();

                if (personObj != null &&
                    personObj.code > 0)
                {
                    results = new Tuple<bool, string>(false, "Failed adding Person, ID number already used by another person.");
                    return results;
                }

                var _newPerson = new Person()
                {
                    name = person.name,
                    surname = person.surname,
                    id_number = person.id_number
                };

                _accountsManagementEntities.Persons.Add(_newPerson);
                _accountsManagementEntities.SaveChanges();
                results = new Tuple<bool, string>(true, "Succesfully added new person!");                
            }
            catch (Exception)
            {
                results = new Tuple<bool, string>(false, "Failed adding person!");
            }

            return results;
        }

        public Tuple<bool, string> UpdatePerson(PersonViewModel person)
        {
            Tuple<bool, string> results;
            var personObj = _accountsManagementEntities
                .Persons.Where(x => x.code == person.code).FirstOrDefault();

            if (personObj != null &&
                personObj.code > 0)
            {
                //-- check if ID number exists for a different person
                var obj = _accountsManagementEntities
                    .Persons.Where(x => x.id_number == person.id_number).FirstOrDefault();

                if (obj != null &&
                   obj.code != personObj.code)
                {
                    results = new Tuple<bool, string>(false, "Failed updating information, ID number belong to another person.");
                    return results;
                }

                try
                {
                    personObj.name = person.name;
                    personObj.surname = person.surname;
                    personObj.id_number = person.id_number;
                    _accountsManagementEntities.Entry(personObj).State = EntityState.Modified;
                    _accountsManagementEntities.SaveChanges();
                    results = new Tuple<bool, string>(true, "Updated person information.");
                }
                catch (Exception)
                {
                    results = new Tuple<bool, string>(true, "Failed Updating person information.");                    
                }
            }
            else
            {
                results = new Tuple<bool, string>(false, "Failed finding person information.");                
            }

            return results;
        }
               
        public IEnumerable<PersonViewModel> SearchPerson(string idNo, string surname, string accountNo)
        {
            var data = _accountsManagementEntities.Database.SqlQuery<PersonViewModel>("dbo.spPersons_SEL_IDNumber_Surname_AccountNumber @id_number, @surname,@account_number",
                new SqlParameter("@id_number", idNo), new SqlParameter("@surname", surname), new SqlParameter("@account_number", accountNo)).ToList<PersonViewModel>();

            return data;
        }

        #endregion

        #region Accounts

        public IEnumerable<AccountsViewModel> GetAllAccounts()
        {
            var data = _accountsManagementEntities.Database.SqlQuery<AccountsViewModel>("dbo.spAccounts_SEL_ALL").ToList<AccountsViewModel>();
            return data;
        }

        public IEnumerable<AccountsViewModel> GetAccountsByPersonCode(int personCode)
        {
            List<AccountsViewModel> viewModelList = new List<AccountsViewModel>();
            
            //-- Use stored procedure
            var data = _accountsManagementEntities.Accounts.Where(x => x.person_code == personCode);
            foreach(Account account in data.ToList())
            {                
                var viewModel = new AccountsViewModel()
                {
                    code = account.code,
                    account_number = account.account_number,
                    person_code = account.person_code,
                    account_active = true,
                    outstanding_balance = account.outstanding_balance
                };

                viewModelList.Add(viewModel);
            }

            return viewModelList;
        }

        public Tuple<bool, string> SaveAccount(AccountsViewModel account)
        {
            Tuple<bool, string> results;
            try
            {
                //validate duplicate id number
                var accountObj = _accountsManagementEntities.Accounts
                    .Where(x => x.account_number == account.account_number).FirstOrDefault();

                if (accountObj != null &&
                    accountObj.code > 0)
                {
                    results = new Tuple<bool, string>(false, "Failed adding Account, account number already exists.");
                    return results;
                }

                var _newAccount = new Account()
                {
                    account_number = account.account_number,
                    person_code = account.person_code,
                    account_active = true, // all new accounts set as active
                    outstanding_balance = account.outstanding_balance
                };

                _accountsManagementEntities.Accounts.Add(_newAccount);
                _accountsManagementEntities.SaveChanges();
                results = new Tuple<bool, string>(true, "Succesfully added new account!");                

            }
            catch(Exception)
            {
                results = new Tuple<bool, string>(false, "Failed creating new account.");
            }

            return results;
        }

        public Tuple<bool, string> UpdateAccount(AccountsViewModel account)
        {
            Tuple<bool, string> results;
            var accountObj = _accountsManagementEntities
                .Accounts.Where(x => x.code == account.code).FirstOrDefault();

            if (accountObj != null &&
                accountObj.code > 0)
            {
                //-- check if account number exists for a different person
                var obj = _accountsManagementEntities
                    .Accounts.Where(x => x.account_number == account.account_number).FirstOrDefault();

                if (obj != null &&
                   obj.code != accountObj.code)
                {
                    results = new Tuple<bool, string>(false, "Failed updating information, account number belong to another person.");
                    return results;
                }

                try
                {
                    accountObj.account_number = account.account_number;
                    //accountObj.outstanding_balance = account.outstanding_balance;
                    //accountObj.account_active = account.account_active;
                    _accountsManagementEntities.Entry(accountObj).State = EntityState.Modified;
                    _accountsManagementEntities.SaveChanges();
                    results = new Tuple<bool, string>(true, "Updated account information.");
                }
                catch (Exception)
                {
                    results = new Tuple<bool, string>(true, "Failed Updating account information.");
                }
            }
            else
            {
                results = new Tuple<bool, string>(false, "Failed finding account information.");
            }

            return results;
        }

        public Tuple<bool, string> CloseAccount(int accountCode)
        {
            Tuple<bool, string> results;
            var accountObj = _accountsManagementEntities.Accounts
                    .Where(x => x.code == accountCode).FirstOrDefault();

            if(accountObj == null)
            { 
                results = new Tuple<bool, string>(false, "Failed closing account, account not found!");
                return results;
            }

            if(accountObj.code > 0 &&
               accountObj.outstanding_balance == 0)
            {
                if(!accountObj.account_active)
                {
                    results = new Tuple<bool, string>(false, "This account has already been closed!");
                    return results;
                }

                try
                {
                    accountObj.account_active = false;
                    _accountsManagementEntities.Entry(accountObj).State = EntityState.Modified;
                    _accountsManagementEntities.SaveChanges();
                    results = new Tuple<bool, string>(true, "Successfully closed the account!");
                }
                catch(Exception)
                {                    
                    results = new Tuple<bool, string>(false, "Failed closing account!");
                }
            }
            else
            {                
                results = new Tuple<bool, string>(false, "Failed closing account, only allowed to close accounts with zero balance!");                
            }
            return results;
        }

        public Tuple<bool, string> ReOpenAccount(int accountCode)
        {
            Tuple<bool, string> results;
            var accountObj = _accountsManagementEntities.Accounts
                    .Where(x => x.code == accountCode).FirstOrDefault();
            
            if (accountObj != null &&
                accountObj.code > 0)
            {
                if (accountObj.account_active)
                {
                    results = new Tuple<bool, string>(false, "This account has already been re-opened!");
                    return results;
                }

                try
                {
                    accountObj.account_active = true;
                    _accountsManagementEntities.Entry(accountObj).State = EntityState.Modified;
                    _accountsManagementEntities.SaveChanges();
                    results = new Tuple<bool, string>(true, "Successfully re-opened the account!");
                }
                catch (Exception)
                {
                    results = new Tuple<bool, string>(false, "Failed re-opening account!");
                }
            }
            else
            {
                results = new Tuple<bool, string>(false, "Failed Re-Opening account, account not found!");                
            }
            return results;
        }

        public bool DeleteAccount(int accountCode)
        {
            bool status = false;

            try
            {
                Account accountObj = new Account { code = accountCode };
                _accountsManagementEntities.Entry(accountObj).State = EntityState.Deleted;
                _accountsManagementEntities.SaveChanges();
                status = true;
            }
            catch (Exception)
            { }

            return status;
        }

        #endregion

        #region Transactions

        public IEnumerable<TransactionsViewModel> GetAllTransactions()
        {
            var data = _accountsManagementEntities.Database.SqlQuery<TransactionsViewModel>("dbo.spTransactions_SEL_ALL").ToList<TransactionsViewModel>();
            return data;
        }

        public IEnumerable<TransactionsViewModel> GetTransactionsByAccountCode(int acountCode)
        {
            List<TransactionsViewModel> viewModelList = new List<TransactionsViewModel>();

            //-- Use stored proc
            var data = _accountsManagementEntities.Transactions.Where(x => x.account_code == acountCode);
            foreach (Transaction transaction in data.ToList())
            {
                var viewModel = new TransactionsViewModel()
                {
                    code = transaction.code,
                    account_code = transaction.account_code,
                    transaction_date = transaction.capture_date,
                    amount = transaction.amount,
                    description = transaction.description
                };

                viewModelList.Add(viewModel);
            }

            return viewModelList;
        }

        public Tuple<bool, string> SaveTransaction(TransactionsViewModel viewModel)
        {
            Tuple<bool, string> results;

            var accountObj = _accountsManagementEntities.Accounts
                .Where(x => x.code == viewModel.account_code).FirstOrDefault();

            if (accountObj != null &&
                accountObj.account_active)
            {
                try
                {
                    if (viewModel.transaction_type == "C")
                    {
                        // Credit
                        accountObj.outstanding_balance += Math.Abs(viewModel.amount);
                    }
                    else
                    {
                        // Debit
                        accountObj.outstanding_balance -= Math.Abs(viewModel.amount);
                    }

                    //-- Use db transaction to add new transaction then

                    var _newtransaction = new Transaction()
                    {
                        account_code = viewModel.account_code,
                        transaction_date = viewModel.transaction_date,
                        capture_date = viewModel.capture_date,
                        amount = viewModel.amount,
                        description = viewModel.description
                    };                


                    using (var dbTrans = _accountsManagementEntities.Database.BeginTransaction())
                    {
                        try
                        {
                            _accountsManagementEntities.Entry(accountObj).State = EntityState.Modified;
                            _accountsManagementEntities.SaveChanges();

                            _accountsManagementEntities.Transactions.Add(_newtransaction);
                            _accountsManagementEntities.SaveChanges();

                            dbTrans.Commit();
                        } catch (Exception)
                        {
                            results = new Tuple<bool, string>(false, "Failed creating transaction!");
                            dbTrans.Rollback();
                        }
                    }

                    results = new Tuple<bool, string>(true, "Succesfully created transaction!");
                }
                catch (Exception)
                {
                    results = new Tuple<bool, string>(false, "Failed creating transaction!");
                }
            }else
            {
                results = new Tuple<bool, string>(false, "Failed finding account, Please check if the account is not closed and re-open it!");
            }
            
            return results;
        }

        public Tuple<bool, string> UpdateTransaction(TransactionsViewModel transaction)
        {
            Tuple<bool, string> results;
            var transactionObj = _accountsManagementEntities
                .Transactions.Where(x => x.code == transaction.code).FirstOrDefault();

            if (transactionObj != null &&
                transactionObj.code > 0)
            {
                try
                {
                    //-- if the user needs to update amount the can create new transaction to Credit/Debit
                    
                    transactionObj.transaction_date = transaction.transaction_date;
                    transactionObj.description = transaction.description;                    
                    transactionObj.capture_date = DateTime.Now;

                    _accountsManagementEntities.Entry(transactionObj).State = EntityState.Modified;
                    _accountsManagementEntities.SaveChanges();
                    results = new Tuple<bool, string>(true, "Successfully updated transaction information!");
                }
                catch (Exception)
                {
                    results = new Tuple<bool, string>(true, "Failed updating transaction information!");
                }
            }
            else
            {
                results = new Tuple<bool, string>(false, "Failed finding transaction information!");
            }

            return results;
        }

        public bool DeleteTransaction(int transactionCode)
        {
            bool status = false;

            try
            {
                Transaction personObj = new Transaction { code = transactionCode };
                _accountsManagementEntities.Entry(personObj).State = EntityState.Deleted;
                _accountsManagementEntities.SaveChanges();
                status = true;
            }
            catch (Exception)
            { }

            return status;
        }
 
        #endregion
    }
}
